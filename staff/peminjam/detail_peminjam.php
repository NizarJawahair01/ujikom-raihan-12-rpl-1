<?php include 'header.php';?>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Table Detail Transaksi</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
                        <li class="breadcrumb-item">Detail Transaksi</li>
                        <li class="breadcrumb-item active">Table Detail Transaksi</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Table Detail Transaksi</h4>
                               
                                <div class="table-responsive">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Inventaris</th>
                                                <th>Jumlah</th>
                                                <th>Status Peminjaman</th>
                                                <th>Id Pegawai</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
<?php
  include "../koneksi.php";
  $id_peminjaman = $_GET ['id_peminjaman'];
  $query = "SELECT * FROM tb_detail_pinjam JOIN tb_inventaris ON tb_inventaris.id_inventaris = tb_detail_pinjam.id_inventaris WHERE tb_detail_pinjam.id_peminjaman='$id_peminjaman'";
  $sql = mysqli_query($koneksi, $query);
  $no=1;
  while($data = mysqli_fetch_array($sql)){
?>
                                            <tr>
                                              <td><?php echo $no++ ?></td>
                                              <td><?php echo $data['nama'] ?></td>
                                              <td><?php echo $data['jumlahh'] ?></td>
                                              <td><?php echo $data['status_peminjaman'] ?></td>
                                              <td><?php echo $data['id_peminjaman'] ?></td>
                                              
                                            </tr>
            
                                <?php
                                    }
                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 Inventaris Skanic </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
   <?php include 'footer.php';?>