       <?php
            include "header.php";
        ?>

       <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Table Inventaris</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
                        <li class="breadcrumb-item">Data Inventaris</li>
                        <li class="breadcrumb-item active">Table Inventaris</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Edit Data Table Inventaris</h4>
                                <div class="">                               
                                 <form action="proses_tambah_meminjam.php" method="post">    
                                    <?php
                            include "../koneksi.php";
                            $result = mysqli_query($koneksi,"SELECT * from tb_inventaris order by id_inventaris desc ");
                            $jsArray = "var id_inventaris = new Array();\n";
                            ?>
                            <div class="form-group"> 
                            <label>Cari Barang</label>
                            <select class="form-control" required="" name="id_inventaris" onchange="changeValue(this.value)">
                            <option selected="selected">
                            <?php 
                            while($row = mysqli_fetch_array($result)){
                              echo "<option value='$row[id_inventaris]'>$row[nama](stock: $row[jumlah])</option>";
                              $jsArray .= "id_inventaris['". $row['id_inventaris']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
 
                            }
                            ?>
                          </option>
                          </select> 
                        </div>

                                    <div class="form-group">
                                     <label>Jumlah Barang :</label>
                           <input type="number" name="jumlahh" class="form-control"  placeholder="Masukan Jumlah Barang..." required=""> 
                                    </div>

                                    <div class="form-group">
                                      <label>Id Peminjam</label>
                                        <?php
                                          $id_peminjaman = $_GET['id_peminjaman'];
                                          include "../koneksi.php";
                                          $select = mysqli_query($koneksi, "SELECT * FROM tb_peminjaman WHERE id_peminjaman = '$id_peminjaman'");
                                          while($show = mysqli_fetch_array($select)){
                                        ?>
                                          <input name="id_peminjaman" type="number" value="<?php echo $show['id_peminjaman'] ?>" class="from-control" required="" readonly>
                                        <?php
                                          }
                                        ?>
                                    </div>

                              </div>
                                <div class="modal-footer">
                                  <button type="submit" class="btn btn-danger waves-effect waves-light">Simpan</button>
                                </div>
                              </div>
                                        <!-- /.modal-content -->
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 Inventaris Skanic </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
        
                                    <!-- /.modal-dialog -->
<?php include 'footer.php';?>