<?php include 'header.php';?>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Table Data Peminjam</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
                        <li class="breadcrumb-item">Data Peminjam</li>
                        <li class="breadcrumb-item active">Table Data Peminjam</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Table Peminjam</h4>
                               
                                <div class="table-responsive">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama</th>
                                                <th>Tanggal Pinjam</th>
                                                <th>Taggal Kembali</th>
                                                <th>Status</th>
                                                <th>Aksi</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            include "../koneksi.php";
                                            
                                            $query = "SELECT * FROM tb_peminjaman INNER JOIN tb_pegawai ON tb_peminjaman.id_pegawai = tb_pegawai.id_pegawai ORDER BY id_peminjaman DESC";
                                            $sql = mysqli_query($koneksi, $query);
                                            $no = 1;
                                            while($data = mysqli_fetch_array($sql)){
                                            ?>
                                                <tr>
                                                    <td><?php echo $no++ ?></td>
                                                    <td><?php echo $data['nama_pegawai'] ?></td>
                                                    <td><?php echo $data['tanggal_pinjam'] ?></td>
                                                    <td><?php echo $data['tanggal_kembali'] ?></td>
                                                    <td><?php echo $data['status_peminjaman'] ?></td>
                                                    <td>
                                                    <a href="detail_peminjam.php?id_peminjaman=<?php echo $data['id_peminjaman']; ?>" type="button" class="btn waves-effect waves-light btn-info">View</a>
                                                    </td>
                                                </tr>
            
                                <?php
                                    }
                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 Inventaris Skanic </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
        <div id="tambah-jenis" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                <form action="proses/tambah_ruangan.php" method="post">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Tambah Pegawai</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                           <div class="modal-body">
                                                    
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Nama Pegawai</label>
                                                        <input type="text" placeholder="Masukan Nama Pegawai" class="form-control" name="nama_pegawai" required="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">NIP</label>
                                                        <textarea class="form-control" placeholder="Masukan NIP" name="nip" required=""></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Alamat</label>
                                                        <input type="text" placeholder="Masukan Alamat" class="form-control" name="alamat" required="">
                                                    </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                                                <button type="submit" class="btn btn-danger waves-effect waves-light">Simpan</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                                </form>

                                    <!-- /.modal-dialog -->
                                </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
   <?php include 'footer.php';?>