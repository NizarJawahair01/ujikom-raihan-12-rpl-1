<?php include 'header.php';?>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Generate Laporan</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
                        
                        <li class="breadcrumb-item active">Generate Laporan</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Generate Laporan</h4>
                               
                                <div class="table-responsive">
                               

                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Nama</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                                <tbody>
            <tr>
                <td>Inventaris</td>
                <td>
                    <div class="col-md-6">
                        <a href="export_inventaris.php" type="button" class="btn waves-effect waves-light btn-info">Excel</a>
                        <a href="pdf_inventaris.php" type="button" class="btn waves-effect waves-light btn-success">PDF</a>
                    </div>
                </td>
            </tr>
            
            <tr>
                <td>Data Pengembalian</td>
                <td>
                    <div class="col-md-6">
                        <a href="export_kembali.php" type="button" class="btn waves-effect waves-light btn-info">Excel</a>
                        <a href="pdf_kembali.php" type="button" class="btn waves-effect waves-light btn-success"class="btn waves-effect waves-light btn-success">PDF</a>
                    </div>
                </td>
            </tr>

            <tr>
                <td>Data Peminjaman</td>
                <td>
                    <div class="col-md-6">
                        <a href="export_pinjam.php" type="button" class="btn waves-effect waves-light btn-info">Excel</a>
                        <a href="pdf_pinjam.php" type="button" class="btn waves-effect waves-light btn-success">PDF</a>
                    </div>
                </td>
            </tr>

            <tr>
                <td>Semua Data</td>
                <td>
                    <div class="col-md-6">
                        <a href="export_all.php" type="button" class="btn waves-effect waves-light btn-info">Excel</a>
                        <a href="pdf_all.php" type="button" class="btn waves-effect waves-light btn-success">PDF</a>
                    </div>
                </td>
            </tr>
        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 Inventaris Skanic </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
<?php include 'footer.php';?>