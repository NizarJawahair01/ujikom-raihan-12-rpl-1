<?php 
include 'header.php';
include '../koneksi.php';
$id_petugas=$_GET['id_petugas'];
$select=mysqli_query($koneksi,"select * from tb_petugas where id_petugas='$id_petugas'");
$data=mysqli_fetch_array($select);
?>

        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Table Petugas</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
                        <li class="breadcrumb-item">Data Petugas</li>
                        <li class="breadcrumb-item active">Table Petugas</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Edit Data Table Petugas</h4>
                                <div class="">                               
                                 <form action="proses/update_petugas.php?id_petugas=<?php echo $data['id_petugas'];?>" method="post">    
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Username</label>
                                                        <input type="text" placeholder="Masukan Username" class="form-control" name="username" value="<?php echo $data['username'];?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Password</label>
                                                        <input type="text" placeholder="Masukan Password" class="form-control" name="password" value="<?php echo $data['password'];?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Nama Petugas</label>
                                                        <input type="text" placeholder="Masukan Nama Petugas" class="form-control" name="nama_petugas" value="<?php echo $data['nama_petugas'];?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Level</label>
                                                        <select class="form-control" name="id_level" required="">
                                                            <option><?php echo $data['id_level'];?></option>
                                                            <option value="2">Admin</option>
                                                            <option value="3">Petugas</option>
                                                            <option value="4">Peminjam</option>
                                                            
                                                        </select>
                                                    </div>
                                            </div>
                                                    
                                            <div class="modal-footer">
                                                <a href="data_petugas.php" type="button" class="btn btn-default waves-effect">Close</a>
                                                <button type="submit" class="btn btn-danger waves-effect waves-light">Simpan</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                                </form>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 Inventaris Skanic </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
        
                                    <!-- /.modal-dialog -->
<?php include 'footer.php';?>