<?php 
include 'header.php';
include '../koneksi.php';
$id_jenis=$_GET['id_jenis'];
$select=mysqli_query($koneksi,"select * from tb_jenis where id_jenis='$id_jenis'");
$data=mysqli_fetch_array($select);
?>

        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Table Data Jenis Barang</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
                        <li class="breadcrumb-item">Data Jenis Barang</li>
                        <li class="breadcrumb-item active">Table Jenis Barang</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Edit Data Table Jenis Barang</h4>
                                <div class="">                               
                                 <form action="proses/update_jenis_barang.php?id_jenis=<?php echo $data['id_jenis'];?>" method="post">    
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Nama Jenis Barang</label>
                                                        <input type="text" placeholder="Masukan Nama Jenis Barang" class="form-control" name="nama_jenis" value="<?php echo $data['nama_jenis'];?>">
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Kode Jenis Barang</label>
                                                        <input type="text" placeholder="Masukan Kode Jenis Barang" class="form-control" name="kode_jenis" value="<?php echo $data['kode_jenis'];?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Keterangan</label>
                                                        <input class="form-control" placeholder="Masukan Keterangan" name="keterangan" value="<?php echo $data['keterangan'];?>"></input>
                                                    </div>
                                                 
                                            <div class="modal-footer">
                                                <a href="data_jenis_barang.php" type="button" class="btn btn-default waves-effect">Close</a>
                                                <button type="submit" class="btn btn-danger waves-effect waves-light">Simpan</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                                </form>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 Inventaris Skanic </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
        
                                    <!-- /.modal-dialog -->
<?php include 'footer.php';?>