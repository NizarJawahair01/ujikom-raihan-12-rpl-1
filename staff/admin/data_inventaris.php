<?php include 'header.php';?>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Table Inventaris</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
                        <li class="breadcrumb-item">Data Inventaris</li>
                        <li class="breadcrumb-item active">Table Inventaris</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Table Inventaris</h4>
                               
                                <div class="table-responsive">
                                <button type="button" class="btn btn-primary" alt="default" data-toggle="modal" data-target="#tambah-jenis" >Tambah Data</button>
                                 <button type="button" class="btn waves-effect waves-light btn-info">Export Excel</button>
                                 <a href="pdf_inventaris.php"><button type="button" class="btn waves-effect waves-light btn-success">Export PDF</button></a>

                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama</th>
                                                <th>Kondisi</th>
                                                <th>Spesifikasi</th>
                                                <th>Keterangan</th>
                                                <th>Jumlah</th>
                                                <th>Nama Jenis</th>
                                                <th>Tanggal Register</th>
                                                <th>Nama Ruang</th>
                                                <th>Kode Inventaris</th>
                                                <th>Nama Petugas</th>
                                                <th>Sumber</th>
                                                <th>Action</th>

                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            include "../koneksi.php";
                                            $no = 1;
                                            $query_mysqli = mysqli_query ($koneksi,"SELECT * from tb_inventaris INNER JOIN tb_jenis on tb_inventaris.id_jenis = tb_jenis.id_jenis INNER JOIN tb_ruang on tb_inventaris.id_ruang = tb_ruang.id_ruang INNER JOIN tb_petugas on tb_inventaris.id_petugas = tb_petugas.id_petugas ORDER BY id_inventaris DESC") or die (
                                                mysql_error());
                                            while($data = mysqli_fetch_array($query_mysqli)){
                                            ?>
                                                <tr>
                                                    <td><?php echo $no++ ?></td>
                                                    <td><?php echo $data['nama'] ?></td>
                                                    <td><?php echo $data['kondisi'] ?></td>
                                                    <td><?php echo $data['spesifikasi'] ?></td>
                                                    <td><?php echo $data['keterangan'] ?></td>
                                                    <td><?php echo $data['jumlah'] ?></td>
                                                    <td><?php echo $data['nama_jenis'] ?></td>
                                                    <td><?php echo $data['tanggal_register'] ?></td>
                                                    <td><?php echo $data['nama_ruang'] ?></td>
                                                    <td><?php echo $data['kode_inventaris'] ?></td>
                                                    <td><?php echo $data['nama_petugas'] ?></td>
                                                    <td><?php echo $data['sumber'] ?></td>
                                                    <td>
                                                        <a href="edit_inventaris.php?id_inventaris=<?php echo $data['id_inventaris'];?>">Edit</a>
                                                    </td>
                                                </tr>
            
                                <?php
                                    }
                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 Inventaris Skanic </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
        <div id="tambah-jenis" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                <form action="proses/tambah_inventaris.php" method="post">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Edit Inventaris</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                           <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Nama Barang</label>
                                                        <input type="text" placeholder="Masukan Nama Barang" class="form-control" name="nama" required="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Kondisi</label>
                                                        <select class="form-control" name="kondisi" required="">
                                                            <option>-:-</option>
                                                            <option value="baik">Baik</option>
                                                            <option value="rusak">Rusak</option>
                                                            
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Spesifikasi</label>
                                                        <input type="text" placeholder="Masukan Spesifikasi" class="form-control" name="spesifikasi" required="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Keterangan</label>
                                                        <input type="text" placeholder="Masukan Keterangan" class="form-control" name="keterangan" required="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Jumlah</label>
                                                        <input type="number" placeholder="Masukan Jumlah" class="form-control" name="jumlah" required="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Id Jenis</label>
                                                        <select class="form-control" name="id_jenis" required="">
                                                            <option value=" " >-:-</option>
                                                                <?php
                                                                    include "../koneksi.php";
                                                                    $query_mysqli = mysqli_query ($koneksi,"SELECT * from tb_jenis") or die (
                                                                        mysql_error());
                                                                    while($data = mysqli_fetch_array($query_mysqli)){
                                                                ?>
                                                            <option value="<?php echo $data['id_jenis'] ?>"><?php echo $data['nama_jenis'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Tanggal Register</label>
                                                        <input type="date" class="form-control" name="tanggal_register" required">
                                                    </div>



                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Id Ruang</label>
                                                        <select class="form-control" name="id_ruang" required="">
                                                            <option value=" " >-:-</option>
                                                                <?php
                                                                    include "../koneksi.php";
                                                                    $query_mysqli = mysqli_query ($koneksi,"SELECT * from tb_ruang") or die (
                                                                        mysql_error());
                                                                    while($data = mysqli_fetch_array($query_mysqli)){
                                                                ?>
                                                            <option value="<?php echo $data['id_ruang'] ?>"><?php echo $data['nama_ruang'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>

                                                    <?php
                                                        $koneksi = mysqli_connect('localhost','root','','ujikom_inventaris');

                                                        $cari_kd = mysqli_query($koneksi,"select max(kode_inventaris) as kode from tb_inventaris");

                                                        $tm_cari= mysqli_fetch_array($cari_kd);
                                                        $kode=substr($tm_cari['kode'], 3,4);

                                                        $tambah=$kode+1;

                                                        if ($tambah<10) {
                                                            $kode_inventaris="BRG000".$tambah;
                                                        } else {
                                                            $kode_inventaris="BRG00".$tambah;
                                                        }
                                                        ?>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Kode Inventaris</label>
                                                        <input type="text" placeholder="Masukan Kode Inventaris" class="form-control" name="kode_inventaris" readonly="" value="<?php echo $kode_inventaris ?>" >
                                                    </div>



                                                    <div class="form-group">
                                                        <label for="message-text" class="control-label">Id Petugas</label>
                                                        <select class="form-control" name="id_petugas" required="">
                                                            <option value=" " >-:-</option>
                                                                <?php
                                                                    include "../koneksi.php";
                                                                    $query_mysqli = mysqli_query ($koneksi,"SELECT * from tb_petugas") or die (
                                                                        mysql_error());
                                                                    while($data = mysqli_fetch_array($query_mysqli)){
                                                                ?>
                                                            <option value="<?php echo $data['id_petugas'] ?>"><?php echo $data['nama_petugas'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Sumber</label>
                                                        <input type="text" placeholder="Masukan Sumber" class="form-control" name="sumber" required="">
                                                    </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                                                <button type="submit" class="btn btn-danger waves-effect waves-light">Simpan</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                                </form>

                                    <!-- /.modal-dialog -->
                                </div>


<?php include 'footer.php';?>