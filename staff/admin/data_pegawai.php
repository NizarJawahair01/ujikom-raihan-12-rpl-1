<?php include 'header.php';?>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Table Pegawai</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
                        <li class="breadcrumb-item">Data Pegawai</li>
                        <li class="breadcrumb-item active">Table Pegawai</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Table Pegawai</h4>
                               
                                <div class="table-responsive">
                                <button type="button" class="btn btn-primary" alt="default" data-toggle="modal" data-target="#tambah-jenis" >Tambah Data</button>
                                 <button type="button" class="btn waves-effect waves-light btn-info">Export Excel</button>
                                 <button type="button" class="btn waves-effect waves-light btn-success">Export PDF</button>

                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pegawai</th>
                                                <th>NIP</th>
                                                <th>Alamat</th>
                                                <th>Action</th>

                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            include "../koneksi.php";
                                            $no = 1;
                                            $query_mysqli = mysqli_query ($koneksi,"SELECT * from tb_pegawai ORDER BY id_pegawai DESC") or die (
                                                mysql_error());
                                            while($data = mysqli_fetch_array($query_mysqli)){
                                            ?>
                                                <tr>
                                                    <td><?php echo $no++ ?></td>
                                                    <td><?php echo $data['nama_pegawai'] ?></td>
                                                    <td><?php echo $data['nip'] ?></td>
                                                    <td><?php echo $data['alamat'] ?></td>
                                                    <td>
                                                    <a href="edit_pegawai.php?id_pegawai=<?php echo $data['id_pegawai'];?>">Edit</a>
                                                    </td>
                                                </tr>
            
                                <?php
                                    }
                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 Inventaris Skanic </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
        <div id="tambah-jenis" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                <form action="proses/tambah_pegawai.php" method="post">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Tambah Pegawai</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                           <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Nama Pegawai</label>
                                                        <input type="text" placeholder="Masukan Nama Pegawai" class="form-control" name="nama_pegawai" required="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">NIP</label>
                                                        <textarea class="form-control" placeholder="Masukan NIP" name="nip" required=""></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Alamat</label>
                                                        <input type="text" placeholder="Masukan Alamat" class="form-control" name="alamat" required="">
                                                    </div>
                                                     <div class="form-group">
                                                        <label for="recipient-name" class="control-label">No. Telepon</label>
                                                        <input type="number" placeholder="Masukan No. Telepon" class="form-control" name="no_telfon" required="">
                                                    </div>
                                                     <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Username</label>
                                                        <input type="text" placeholder="Masukan Username" class="form-control" name="username" required="">
                                                    </div>
                                                     <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Password</label>
                                                        <input type="text" placeholder="Masukan Password" class="form-control" name="password" required="">
                                                    </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                                                <button type="submit" class="btn btn-danger waves-effect waves-light">Simpan</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                                </form>

                                    <!-- /.modal-dialog -->
                                    </div>
                                                </form>

                                    <!-- /.modal-dialog -->
                                </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
   <?php include 'footer.php';?>