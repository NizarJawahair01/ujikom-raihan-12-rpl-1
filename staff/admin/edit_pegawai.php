<?php 
include 'header.php';
include '../koneksi.php';
$id_pegawai=$_GET['id_pegawai'];
$select=mysqli_query($koneksi,"select * from tb_pegawai where id_pegawai='$id_pegawai'");
$data=mysqli_fetch_array($select);
?>

        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Table Data Pegawai</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
                        <li class="breadcrumb-item">Data Pegawai</li>
                        <li class="breadcrumb-item active">Table Pegawai</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Edit Pegawai</h4>
                                <div class="">                               
                                 <form action="proses/update_pegawai.php?id_pegawai=<?php echo $data['id_pegawai'];?>" method="post">    
                                                    
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Nama Pegawai</label>
                                                        <input type="text" placeholder="Masukan Nama Pegawai" class="form-control" name="nama_pegawai" value="<?php echo $data['nama_pegawai'];?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">NIP</label>
                                                        <input class="form-control" placeholder="Masukan NIP" name="nip" value="<?php echo $data['nip'];?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Alamat</label>
                                                        <input class="form-control" placeholder="Masukan Alamat" name="alamat" value="<?php echo $data['alamat'];?>">
                                                    </div>
                                                 
                                            <div class="modal-footer">
                                                <a href="data_pegawai.php" type="button" class="btn btn-default waves-effect">Close</a>
                                                <button type="submit" class="btn btn-danger waves-effect waves-light">Simpan</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                                </form>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 Inventaris Skanic </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
        
                                    <!-- /.modal-dialog -->
<?php include 'footer.php';?>