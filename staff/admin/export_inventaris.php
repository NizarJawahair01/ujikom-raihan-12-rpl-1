<!DOCTYPE html>
<html>
<head>
  <title>Inventaris SMK</title>
</head>
<body>
  <style type="text/css">
  body{
    font-family: sans-serif;
  }
  table{
    margin: 20px auto;
    border-collapse: collapse;
  }
  table th,
  table td{
    border: 1px solid #3c3c3c;
    padding: 3px 8px;

  }
  a{
    background: blue;
    color: #fff;
    padding: 8px 10px;
    text-decoration: none;
    border-radius: 2px;
  }
  </style>

  <?php
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Data Inventaris.xls");
  ?>

  <center>
    <h1>SMK NEGERI 1 CIOMAS</h1><br>
    <h3>Telp : (0251)8631261. JL. Raya Laladon Ds.Laladon, Kec.Ciomas Kab.Bogor Kode Pos. 16610</h3></br>
    <h3>Email : smkn1_ciomas@yahoo.co.id, Website : www.smkn1ciomas.sch.id</h3>
  </center>

  <table border="1">
   <thead>
    <tr>
       <th>No</th>
                                            <th>Nama Barang</th>
                                            <th>Kondisi</th> 
                                            <th>Keterangan</th>
                                            <th>Jumlah</th>
                                            <th>Nama Jenis</th>
                                            <th>Tanggal Register</th>
                                            <th>Nama Ruang</th>
                                            <th>Kode Inventaris</th>
                                            <th>Nama Petugas</th> 
    </tr>
     </tr>
                                    </thead>
                                    <tbody>
                                    <?php
            include '../koneksi.php';
            $no=1;
$query_mysqli = mysqli_query ($koneksi,"SELECT * from tb_inventaris INNER JOIN tb_jenis on tb_inventaris.id_jenis = tb_jenis.id_jenis INNER JOIN tb_ruang on tb_inventaris.id_ruang = tb_ruang.id_ruang INNER JOIN tb_petugas on tb_inventaris.id_petugas = tb_petugas.id_petugas ORDER BY id_inventaris DESC");
            while ($data = mysqli_fetch_array($query_mysqli))
            {
                ?>

                                        <tr>
                  <td><?php echo $no++; ?></td>
                    <td><?php echo $data['nama']; ?></td>
                    <td><?php echo $data['kondisi']; ?></td> 
                    <td><?php echo $data['keterangan']; ?></td>
                    <td><?php echo $data['jumlah']; ?></td>
                    <td><?php echo $data['nama_jenis']; ?></td>
                    <td><?php echo $data['tanggal_register']; ?></td>
                    <td><?php echo $data['nama_ruang']; ?></td>
                    <td><?php echo $data['kode_inventaris']; ?></td>
                    <td><?php echo $data['nama_petugas']; ?></td>        
                   

       

                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                                 
</body>
</html>