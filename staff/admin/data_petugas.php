<?php include 'header.php';?>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Table Petugas</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
                        <li class="breadcrumb-item">Data Petugas</li>
                        <li class="breadcrumb-item active">Table Petugas</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Table Petugas</h4>
                               
                                <div class="table-responsive">
                                <button type="button" class="btn btn-primary" alt="default" data-toggle="modal" data-target="#tambah-jenis" >Tambah Data</button>
                                 <button type="button" class="btn waves-effect waves-light btn-info">Export Excel</button>
                                 <button type="button" class="btn waves-effect waves-light btn-success">Export PDF</button>

                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Username</th>
                                                <th>Password</th>
                                                <th>Nama Petugas</th>
                                                <th>E-mail</th>
                                                <th>Level</th>
                                                <th>Banned</th>
                                                <th>Logintime</th>
                                                <th>Action</th>

                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            include "../koneksi.php";
                                            $no = 1;
                                            $query_mysqli = mysqli_query ($koneksi,"SELECT * from tb_petugas ORDER BY id_petugas DESC") or die (
                                                mysql_error());
                                            while($data = mysqli_fetch_array($query_mysqli)){
                                            ?>
                                                <tr>
                                                    <td><?php echo $no++ ?></td>
                                                    <td><?php echo $data['username'] ?></td>
                                                    <td><?php echo $data['password'] ?></td>
                                                    <td><?php echo $data['nama_petugas'] ?></td>
                                                    <td><?php echo $data['email'] ?></td>
                                                    <td><?php echo $data['id_level'] ?></td>
                                                    <td><?php echo $data['banned'] ?></td>
                                                    <td><?php echo $data['logintime'] ?></td>
                                                   
                                                    
                                                    <td>
                                                        <a href="edit_petugas.php?id_petugas=<?php echo $data['id_petugas'];?>">Edit</a>
                                                    </td>
                                                </tr>
            
                                <?php
                                    }
                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 Inventaris Skanic </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
        <div id="tambah-jenis" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                <form action="proses/tambah_petugas.php" method="post">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Edit Petugas</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                           <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Username</label>
                                                        <input type="text" placeholder="Masukan Username" class="form-control" name="username" required="">
                                                    </div>
                                                   <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Password</label>
                                                        <input type="text" placeholder="Masukan Password" class="form-control" name="password" required="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Nama Petugas</label>
                                                        <input type="text" placeholder="Masukan Nama Petugas" class="form-control" name="nama_petugas" required="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Email</label>
                                                        <input type="text" placeholder="Masukan Email" class="form-control" name="email" required="">
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Level</label>
                                                        <input type="text" placeholder="Masukan Level" class="form-control" name="id_level" required="">
                                                    </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                                                <button type="submit" class="btn btn-danger waves-effect waves-light">Simpan</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                                </form>

                                    <!-- /.modal-dialog -->
                                </div>


<?php include 'footer.php';?>