<?php include 'header.php';?>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Table Ruangan</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
                        <li class="breadcrumb-item">Data Ruangan</li>
                        <li class="breadcrumb-item active">Table Ruangan</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Table Ruangan</h4>
                               
                                <div class="table-responsive">
                                <button type="button" class="btn btn-primary" alt="default" data-toggle="modal" data-target="#tambah-jenis" >Tambah Data</button>
                                 <button type="button" class="btn waves-effect waves-light btn-info">Export Excel</button>
                                 <button type="button" class="btn waves-effect waves-light btn-success">Export PDF</button>

                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Ruangan</th>
                                                <th>Kode Ruangan</th>
                                                <th>Keterangan</th>
                                                <th>Action</th>

                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            include "../koneksi.php";
                                            $no = 1;
                                            $query_mysqli = mysqli_query ($koneksi,"SELECT * from tb_ruang ORDER BY id_ruang DESC") or die (
                                                mysql_error());
                                            while($data = mysqli_fetch_array($query_mysqli)){
                                            ?>
                                                <tr>
                                                    <td><?php echo $no++ ?></td>
                                                    <td><?php echo $data['nama_ruang'] ?></td>
                                                    <td><?php echo $data['kode_ruang'] ?></td>
                                                    <td><?php echo $data['keterangan'] ?></td>
                                                    <td>
                                                    <a href="edit_ruangan.php?id_ruang=<?php echo $data['id_ruang'];?>">Edit</a>
                                                    </td>
                                                </tr>
            
                                <?php
                                    }
                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 Inventaris Skanic </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
        <div id="tambah-jenis" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                <form action="proses/tambah_ruangan.php" method="post">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Tambah Ruangan</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                           <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Nama Ruangan</label>
                                                        <input type="text" placeholder="Masukan Nama Ruangan" class="form-control" name="nama_ruang" required="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Kode Ruangan</label>
                                                        <input type="text" placeholder="Masukan Kode Ruang" class="form-control" name="kode_ruang" required="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Keterangan</label>
                                                        <textarea class="form-control" placeholder="Masukan Keterangan" name="keterangan" required=""></textarea>
                                                    </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                                                <button type="submit" class="btn btn-danger waves-effect waves-light">Simpan</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                                </form>

                                    <!-- /.modal-dialog -->
                                </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
   <?php include 'footer.php';?>