<?php 
include 'header.php';
include '../koneksi.php';
$id_inventaris=$_GET['id_inventaris'];
$select=mysqli_query($koneksi,"select * from tb_inventaris INNER JOIN tb_jenis on tb_inventaris.id_jenis = tb_jenis.id_jenis INNER JOIN tb_ruang on tb_inventaris.id_ruang = tb_ruang.id_ruang INNER JOIN tb_petugas on tb_inventaris.id_petugas = tb_petugas.id_petugas where id_inventaris='$id_inventaris'");
$data=mysqli_fetch_array($select);
?>

        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Table Inventaris</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
                        <li class="breadcrumb-item">Data Inventaris</li>
                        <li class="breadcrumb-item active">Table Inventaris</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Edit Data Table Inventaris</h4>
                                <div class="">                               
                                 <form action="proses/update_inventaris.php?id_inventaris=<?php echo $data['id_inventaris'];?>" method="post">    
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Nama Barang</label>
                                                        <input type="text" placeholder="Masukan Nama Barang" class="form-control" name="nama" value="<?php echo $data['nama'];?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Kondisi</label>
                                                        <select class="form-control" name="kondisi" required="">
                                                            <option><?php echo $data['kondisi'];?></option>
                                                            <option value="baik">Baik</option>
                                                            <option value="rusak">Rusak</option>
                                                            
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Spesifikasi</label>
                                                        <input type="text" placeholder="Masukan spesifikasi" class="form-control" name="spesifikasi" value="<?php echo $data['spesifikasi'];?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Keterangan</label>
                                                        <input class="form-control" placeholder="Masukan Keterangan" name="keterangan" value="<?php echo $data['keterangan'];?>"></input>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Jumlah</label>
                                                        <input type="text" placeholder="Masukan Jumlah" class="form-control" name="jumlah" value="<?php echo $data['jumlah'];?>">
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Kode</label>
                                                        <input type="text" placeholder="Masukan Kode" class="form-control" name="kode_inventaris" value="<?php echo $data['kode_inventaris'];?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Tanggal Register</label>
                                                        <input type="date" id="date" class="form-control" name="tanggal_register" 
                                                        value="<?php echo $data['tanggal_register'];?>" />
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Id Jenis</label>
                                                        <select class="form-control" name="id_jenis" required=""> 
                                                                <?php
                                                                    include "../koneksi.php";
                                                                    $query_mysqli = mysqli_query ($koneksi,"SELECT * from tb_jenis") or die (mysql_error());
                                                                    while($data = mysqli_fetch_array($query_mysqli)){
                                                                ?>
                                                            <option><?php echo $data['id_jenis'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>



                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Id Ruang</label>
                                                        <select class="form-control" name="id_ruang" required="">
                                                                <?php
                                                                    include "../koneksi.php";
                                                                    $query_mysqli = mysqli_query ($koneksi,"SELECT * from tb_ruang") or die (mysql_error());
                                                                    while($data = mysqli_fetch_array($query_mysqli)){
                                                                ?> 
                                                            <option><?php echo $data['id_ruang'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="message-text" class="control-label">Id Petugas</label>
                                                        <select class="form-control" name="id_petugas" required=""> 
                                                                <?php
                                                                    include "../koneksi.php";
                                                                    $query_mysqli = mysqli_query ($koneksi,"SELECT * from tb_petugas") or die (
                                                                        mysql_error());
                                                                    while($data = mysqli_fetch_array($query_mysqli)){
                                                                ?>
                                                            <option><?php echo $data['id_petugas'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                            </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Sumber</label>
                                                        <input type="text" placeholder="Masukan sumber" class="form-control" name="sumber" value="<?php echo $data['sumber'];?>">
                                                    </div>
                                            <div class="modal-footer">
                                                <a href="data_inventaris.php" type="button" class="btn btn-default waves-effect">Close</a>
                                                <button type="submit" class="btn btn-danger waves-effect waves-light">Simpan</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                                </form>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 Inventaris Skanic </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
        
                                    <!-- /.modal-dialog -->
<?php include 'footer.php';?>