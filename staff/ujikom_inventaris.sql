-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05 Apr 2019 pada 19.52
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujikom_inventaris`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_detail_pinjam`
--

CREATE TABLE `tb_detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlahh` varchar(30) NOT NULL,
  `status_peminjaman` enum('dipinjam','dikembalikan') NOT NULL,
  `id_peminjaman` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tb_detail_pinjam`
--

INSERT INTO `tb_detail_pinjam` (`id_detail_pinjam`, `id_inventaris`, `jumlahh`, `status_peminjaman`, `id_peminjaman`) VALUES
(15, 3, '1', 'dikembalikan', 201248),
(16, 1, '2', 'dikembalikan', 201249),
(17, 8, '2', 'dikembalikan', 201250),
(18, 5, '1', 'dikembalikan', 201251),
(19, 4, '5', 'dikembalikan', 201252),
(20, 6, '10', 'dipinjam', 201253),
(21, 7, '2', 'dipinjam', 201254),
(22, 3, '5', 'dipinjam', 201255),
(23, 5, '4', 'dipinjam', 201256),
(24, 7, '2', 'dipinjam', 201257),
(25, 6, '5', 'dipinjam', 201258);

--
-- Trigger `tb_detail_pinjam`
--
DELIMITER $$
CREATE TRIGGER `pengembalian` BEFORE UPDATE ON `tb_detail_pinjam` FOR EACH ROW UPDATE tb_inventaris SET jumlah = jumlah+NEW.jumlahh WHERE id_inventaris = NEW.id_inventaris and NEW.status_peminjaman='dikembalikan'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `pengurangan_stok` AFTER INSERT ON `tb_detail_pinjam` FOR EACH ROW UPDATE tb_inventaris SET jumlah = jumlah - NEW.jumlahh WHERE id_inventaris = NEW.id_inventaris and NEW.status_peminjaman='dipinjam'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_inventaris`
--

CREATE TABLE `tb_inventaris` (
  `id_inventaris` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kondisi` enum('Baik','Rusak') NOT NULL,
  `spesifikasi` text NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` varchar(10) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `sumber` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_inventaris`
--

INSERT INTO `tb_inventaris` (`id_inventaris`, `nama`, `kondisi`, `spesifikasi`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_petugas`, `sumber`) VALUES
(1, 'Proyektor', 'Baik', 'BenQ 1080p', 'Baik', '20', 18, '2019-04-02', 2, 'BRG0001', 4, 'Kemdikbud'),
(3, 'Laptop', 'Baik', 'ASUS X454Y', 'LAB RPL 1', '25', 18, '2019-04-05', 2, 'BRG0002', 4, 'Kemdikbud'),
(4, 'Keyboard', 'Baik', 'Razer Cortex A98', 'LAB RPL 1', '30', 18, '2019-04-05', 2, 'BRG0003', 4, 'SMKN 1 Ciomas'),
(5, 'Mini Speaker', 'Baik', 'Dolby Digital Sounds', 'LAB RPL 1', '16', 18, '2019-04-05', 2, 'BRG0004', 4, 'SMKN 1 Ciomas'),
(6, 'Kabel HDMI', 'Baik', 'Term Kuningan', 'LAB RPL 1', '15', 18, '2019-04-05', 2, 'BRG0005', 4, 'Kemdikbud'),
(7, 'ACCU', 'Baik', 'GS Astra', 'Bengkel TKR', '6', 19, '2019-04-05', 5, 'BRG0006', 4, 'Kemdikbud'),
(8, 'Mesin Las', 'Baik', 'FireBolt 88', 'Bengkel TPL', '10', 19, '2019-04-05', 6, 'BRG0007', 4, 'Kemdikbud');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jenis`
--

CREATE TABLE `tb_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(20) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jenis`
--

INSERT INTO `tb_jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(18, 'Elektronik', 'ELK01', 'Barang Elektronik'),
(19, 'Bengkel', 'BKL02', 'Barang Bengkel'),
(20, 'Kelas', 'KLS03', 'Barang Kelas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_level`
--

CREATE TABLE `tb_level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_level`
--

INSERT INTO `tb_level` (`id_level`, `nama_level`) VALUES
(2, 'admin'),
(3, 'petugas'),
(4, 'peminjam');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pegawai`
--

CREATE TABLE `tb_pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `no_telfon` int(15) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pegawai`
--

INSERT INTO `tb_pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`, `no_telfon`, `username`, `password`) VALUES
(111000112, 'Raihan Nizar Jawahair', '18983193001', 'Ciherang', 88219, 'nizar', 'nizar'),
(111000113, 'Shania Junianatha', '1224664220', 'Jakarta', 882675541, 'shanju', 'shanju'),
(111000114, 'Mohammed Salah', '1131313501122', 'Mesir', 88743535, 'mosalah', 'mosalah'),
(111000115, 'Sadio Mane', '11543222577', 'Senegal', 342424246, 'mane', 'mane'),
(111000116, 'Roberto Firmino', '1901838113', 'Brazil', 2147483647, 'firmino', 'firmino');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_peminjaman`
--

CREATE TABLE `tb_peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` enum('dipinjam','dikembalikan') NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_peminjaman`
--

INSERT INTO `tb_peminjaman` (`id_peminjaman`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_pegawai`) VALUES
(201248, '2019-04-06', '2019-04-10', 'dikembalikan', 111000112),
(201249, '2019-04-06', '2019-04-09', 'dikembalikan', 111000113),
(201250, '2019-04-06', '2019-04-10', 'dikembalikan', 111000115),
(201251, '2019-04-06', '2019-04-08', 'dikembalikan', 111000114),
(201252, '2019-04-06', '2019-04-10', 'dikembalikan', 111000116),
(201253, '2019-04-06', '2019-04-11', 'dipinjam', 111000112),
(201254, '2019-04-06', '2019-04-10', 'dipinjam', 111000112),
(201255, '2019-04-06', '2019-04-08', 'dipinjam', 111000114),
(201256, '2019-04-06', '2019-04-11', 'dipinjam', 111000113),
(201257, '2019-04-06', '2019-04-06', 'dipinjam', 111000115),
(201258, '2019-04-06', '2019-04-12', 'dipinjam', 111000116);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_petugas`
--

CREATE TABLE `tb_petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  `banned` enum('N','Y') NOT NULL,
  `logintime` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_petugas`
--

INSERT INTO `tb_petugas` (`id_petugas`, `username`, `password`, `nama_petugas`, `email`, `id_level`, `banned`, `logintime`) VALUES
(4, 'admin', 'admin', 'Admin', '', 2, 'N', 0),
(5, 'petugas', 'petugas', 'Petugas', '', 3, 'N', 0),
(6, 'peminjam', 'peminjam', 'Peminjam', '', 4, 'N', 0),
(7, 'nama', 'fsaTfl51', 'nama', 'raihannizar2001@gmail.com', 2, 'N', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_ruang`
--

CREATE TABLE `tb_ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(20) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_ruang`
--

INSERT INTO `tb_ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(2, 'LAB RPL 1', 'LAB-01', 'LAB RPL 1'),
(3, 'LAB RPL 2', 'LAB-02', 'LAB RPL 2'),
(4, 'LAB RPL 3', 'LAB-03', 'LAB RPL 3'),
(5, 'Bengkel TKR', 'BKL-01', 'Bengkel TKR'),
(6, 'Bengkel TPL', 'BKL-02', 'Bengkel TPL'),
(7, 'Lab Animasi 1', 'LAB-04', 'Lab Animasi 1'),
(8, 'Lab Animasi 2', 'LAB-05', 'Lab Animasi 2'),
(9, 'Studio Animasi', 'STD-01', 'Studio Animasi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_detail_pinjam`
--
ALTER TABLE `tb_detail_pinjam`
  ADD PRIMARY KEY (`id_detail_pinjam`),
  ADD KEY `id_inventaris` (`id_inventaris`),
  ADD KEY `id_peminjaman` (`id_peminjaman`);

--
-- Indexes for table `tb_inventaris`
--
ALTER TABLE `tb_inventaris`
  ADD PRIMARY KEY (`id_inventaris`),
  ADD KEY `id_jenis` (`id_jenis`),
  ADD KEY `id_ruang` (`id_ruang`),
  ADD KEY `id_petugas` (`id_petugas`);

--
-- Indexes for table `tb_jenis`
--
ALTER TABLE `tb_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `tb_level`
--
ALTER TABLE `tb_level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `tb_pegawai`
--
ALTER TABLE `tb_pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `tb_peminjaman`
--
ALTER TABLE `tb_peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indexes for table `tb_petugas`
--
ALTER TABLE `tb_petugas`
  ADD PRIMARY KEY (`id_petugas`),
  ADD KEY `id_level` (`id_level`);

--
-- Indexes for table `tb_ruang`
--
ALTER TABLE `tb_ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_detail_pinjam`
--
ALTER TABLE `tb_detail_pinjam`
  MODIFY `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `tb_inventaris`
--
ALTER TABLE `tb_inventaris`
  MODIFY `id_inventaris` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_jenis`
--
ALTER TABLE `tb_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tb_level`
--
ALTER TABLE `tb_level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_pegawai`
--
ALTER TABLE `tb_pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111000117;
--
-- AUTO_INCREMENT for table `tb_peminjaman`
--
ALTER TABLE `tb_peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201259;
--
-- AUTO_INCREMENT for table `tb_petugas`
--
ALTER TABLE `tb_petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_ruang`
--
ALTER TABLE `tb_ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
