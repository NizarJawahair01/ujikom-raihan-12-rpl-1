<!DOCTYPE html>
<html>
<head>
	<title>Login Inventaris</title>
	<link rel="stylesheet" href="style_login.css" type="text/css">
</head>
<body>

<form action="proses_login.php" method="post">
  <div class="imgcontainer">
    <img src="admin/img/logo.png" width="80" height="120"class="avatar">
	<h2>Login Inventaris </h2>
  </div>

  <div class="container">
    <label><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="username" required>

    <label><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" required>
        
    <button type="submit" name="login">Masuk</button>
  </div>

  <div class="container" style="background-color:#f1f1f1">
    <span class="psw">Lupa <a href="forget.php">password?</a></span>
  </div>
</form>

</body>
</html>
